﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ws_api_aula_manager.Models;

namespace ws_api_aula_manager.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/v1/alumnos")]
    public class AlumnosController : ApiController
    {

        [HttpGet]
        [Route("obteneralumnos/{page:int}")]
        public IHttpActionResult ObtenerAlumnos([FromUri] int page )
        {
            SqlDataReader reader = null;
            List<Alumno> searchresults = new List<Alumno>();

            using ( SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connDB"].ConnectionString) )
            {
                SqlCommand cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "sp_am_obtener_alumnos",
                    Connection = conn
                };
                cmd.Parameters.AddWithValue("@i_page", page);

                try
                {
                    conn.Open();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Alumno al = new Alumno
                        {
                            Al_id = (int)reader[1],
                            Al_nombre = reader[2].ToString(),
                            Al_apellidos = reader[3].ToString(),
                            Al_genero = reader[4].ToString(),
                            Al_fecha_nacimiento = (DateTime)reader[5]
                        };

                        searchresults.Add(al);
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
                
            }

            return Json(searchresults);
        }
    }
}

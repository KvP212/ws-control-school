﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ws_api_aula_manager.Models
{
    public class Alumno
    {
        public int Al_id { get; set; }
        public string Al_nombre { get; set; }
        public string Al_apellidos { get; set; }
        public string Al_genero { get; set; }
        public DateTime Al_fecha_nacimiento { get; set; }

    }
}